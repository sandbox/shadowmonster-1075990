<?php
include_once('hostcare.features.inc');

/**
 * @file
 * Hostcare Module which is a base for all other modules supporting
 * Hostcare Servers Care System. This module take care about global 
 * functions and API connections with other modules.
 *
 * @author Martin Zbozien <martin@hostcare.co.uk>
 * @author Mark Thord <mark.thord@gmail.com>
 * 
 */

/**
 * Implementation of hook_help().
 */
function hostcare_help($path, $arg) {
        switch ($path) {
                case "admin/help/hostcare":
                        $output = '<p>' .   t("hostcare module comming soon...")  . '</p>';
                break;
                }
        return $output;
        }

/**
 * Implementation of hook_perm().
 */
function hostcare_perm() {
        return array(
                'administer hostcare module',
                'edit own hostcare server',
                'create hostcare server',
                'view hc_server_list-domains',
                'administer hc accounts',
                );
        }

/**
* Implementation of hook_access() . 
*/
function hostcare_access($op, $node) {

        global $user;

        if ($op == 'create') {
                // Only users with permission to do so may create this node type . 
                return user_access('create hostcare server');
                }

        // Users who create a node may edit or delete it later, assuming they have the
        // necessary permissions . 
        if ($op == 'update' || $op == 'delete') {

                if (user_access('edit own hostcare server') && ($user->uid == $node->uid)) {

                        return TRUE;
                        }
                }
        }

/**
 * Implementation of hook_form().
 */
function hostcare_form(&$node, $form_state) {
        $type = node_get_types('type', $node);

        if ($type->type == 'hostcare_server') {
                $form['title'] = array(
                        '#type' => 'textfield',
                        '#title' => t('Domain Name or IP'),
                        '#required' => TRUE,
                        '#default_value' => $node->title,
                        '#weight' => -15
                        );
                }
        if ($type->type == 'hostcare_v_server') {
                $form['title'] = array(
                        '#type' => 'textfield',
                        '#title' => t('Domain Name'),
                        '#required' => TRUE,
                        '#default_value' => $node->title,
                        '#weight' => -15
                        );
                }
        return $form;
        }

/**
 * Implementation of hook_nodeapi().
 */
function hostcare_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {

        switch ($op) {

                case 'presave':
                        if ($node->type == 'hostcare_server') {
                                $node->field_hc_server_pass[0]['value'] = encrypt($node->field_hc_server_pass[0]['value']);
                                }
                break;
                case 'view':
                        global $user;
                        if ($node->type == 'hostcare_server' and user_access('view hc_server_list-domains', $user)) {
                                $res = hc_list_servers($node->nid);
                                $out = hc_out_convert($res['Server_out'], $res['Server_command']);
                                $out = hc_list_domains($out, $node->nid);

                                $node->content['hc_server_list-domains'] = array(
                                        '#value' => theme('table', array('Domain', 'Username', 'Description', 'Client', 'Options'), $out), 
                                        '#weight' => 10,
                                        );
                                }
                break;
                }
        }

/**
 * Implementation of hook_form_alter().
 * @todo: put AES checking somewhere in global clasule
 * @todo: fix why title and field_hc_server_ref won't be disabled or read only,
 *  maybe it is problem with template
 */
function hostcare_form_alter(&$form, $form_state, $form_id) {

        // If AES encryption is of show message and turn of buttons
        if ($form_id == 'hostcare_server_node_form' or $form_id == 'hostcare_v_server_node_form') {
                if (hostcare_check_encrypt() == FALSE) {
                        drupal_set_message(t('Hostcare System require have correctly set-up Mcrypt AES 256 encryption on Encrypt Module'), 'error');
                        unset($form['buttons']['submit']);
                        unset($form['buttons']['preview']);
                        }
                if (hc_get_servers() == NULL) {
                        drupal_set_message(t('You must first create Server to can add Virtual Server'), 'error');
                        }
                }

        //Add domain name as default into form
        if ($form_id == 'hostcare_v_server_node_form') {

                if ($_GET['domain'] != NULL) {

                        $server = node_load($_GET['server']);

                        $form['title']['#default_value'] = $_GET['domain'];
                        //$form['title']['#attributes'] = array('readonly' => 'readonly');


                        $form['field_hc_server_ref']['#default_value'][0]['nid'] = $_GET['server'];
                        //$form['field_hc_server_ref']['#disabled'] = TRUE;
                        }
                }

        //remove links from fields managament
        if ($form_id == 'content_field_overview_form') {

                unset($form['field_hc_server_login']['remove']);
                unset($form['field_hc_server_port']['remove']);
                unset($form['field_hc_server_pass']['remove']);

                unset($form['field_hc_server_ref']['remove']);
                unset($form['field_hc_server_client']['remove']);
                }
        }

/**
 * List Domains of Server instance from database.
 *
 * At them moment just add some links options to them when listing
 *
 * @param $out
 *   table with domains data from Virtualmin after conversion
 * @param $nid
 *   Node ID
 *
 * @return $out
 *  return table data with additional columns
 *
 * @see hostcare_nodeapi
 *   
 */
function hc_list_domains($out, $nid) {


        foreach ($out as $o_key => $v_out) {

                $sql = "SELECT node.nid AS nid, 
                                node.title AS title
                        FROM node node 
                        WHERE type = 'hostcare_v_server' 
                                AND title = '%s'";

                $result = db_query($sql, trim($out[$o_key]['Domain']));
                $fetch = db_fetch_array($result) ;

                if ($fetch == NULL) {

                        $out[$o_key]['Client'] = '';
                        $out[$o_key]['Options'] = '<a href="/node/add/hostcare-v-server?domain=' . $out[$o_key]['Domain'] . '&server=' . $nid . '">Convert to Node</a>';
                        }
                else {

                        $v_server = node_load($fetch['nid']);
                        $client = user_load($v_server->field_hc_server_client[0]['uid']);


                        $out[$o_key]['Client'] = '<a href="/user/' . $client->uid . '">' . $client->name . '</a>';
                        $out[$o_key]['Options'] = '<a href="/node/' . $v_server->nid . '">' . t('View') . '</a>';
                        }
                }

        return $out;
        }

/**
 * Get Virtual Servers from DB
 *
 * We need list them correctly to can put as options into form
 *
 * @return array $out
 *  return simple table with domains list
 *   
 */
function hc_get_v_servers() {
        global $user;

        $sql = "SELECT node.nid AS nid,
                        node.title AS title
                FROM node node 
                INNER JOIN content_type_hostcare_v_server node_data_field_hc_server_client ON node.vid = node_data_field_hc_server_client.vid
                WHERE (node.type in ('hostcare_v_server')) AND (node_data_field_hc_server_client.field_hc_server_client_uid = '%d')";


        $result = db_query($sql, $user->uid);

        while ($fetch = db_fetch_array($result)) {
                $vserver[] = $fetch['nid'];
                }

        return $vserver;
        }

/**
 * Get Servers from DB
 *
 * We need list them correctly to can put as options into form
 *
 * @return array $out
 *  return simple table with servers list
 *   
 */
function hc_get_servers() {
        global $user;

        $sql = "SELECT node.nid AS nid,
                        node.title AS title
                FROM node node 
                WHERE node.type = 'hostcare_server' AND uid = '%d'";


        $result = db_query($sql, $user->uid);

        while ($fetch = db_fetch_array($result)) {
                $server[] = $fetch['nid'];
                }

        return $server;
        }

function hc_get_server_by_domain($domain) {
        $sql = "SELECT node.nid AS nid, 
                        node.title AS node_title 
                FROM node node  
                WHERE (node.type in ('hostcare_v_server')) 
                        AND ((node.title) = ('%s')) ";

        $result = db_query($sql, trim($domain));

        $v_server = db_fetch_array($result);

        return $v_server;
        }

/**
 * Check if Encrypt module working with AES encryption
 */
function hostcare_check_encrypt() {
        if (variable_get('encrypt_default_method', 'mcrypt_rij_256') == 'mcrypt_rij_256') {
                return TRUE;
                }
        else {
                return FALSE;
                }
        }


/**
 * Get user name scheme setting from Virtualmin template setting.
 *
 * It's simple for now as we need only one setting to know, It also
 * put data from conversion functions into fields.
 * Example now title will be 'username.domain' and not only username
 *
 * @param $node
 *   node with represent the account of email or ftp
 * @param $vserver_node
 *   node with Virtual Server data
 * @param $server_nid
 *   Node ID of Server
 *
 * @return $out
 *   
 */
function hc_get_account_username($node, $vserver_node, $server_nid) {
        $data_in['title'] = $node->title;
        $data_in['domain'] = $vserver_node->title;

        $set = hc_get_vserver_template_set($server_nid, NULL, 'append_style');
        $out = hc_vserver_setting_decode($data_in, 'append_style', $set);

        return $out;
        }


//some code for disable elements
//Orig & Thanks to: http://drupal.org/node/357328#comment-5599826

/**
* Custom after_build callback handler.
*/
function hc_after_build_readonly($form, &$form_state) {
        hc_disable_components( $form );
        return $form;
        }

/**
* Recursively set the disabled attribute to all
* basic (html) elements below the given FAPI structure
* and only to those.
*/
function hc_disable_components( &$elements ) {

        foreach (element_children($elements) as $key) {
                if (isset($elements[$key]) && $elements[$key]) {
                        // Recurse all children
                        hc_disable_components( $elements[$key] );
                        }
                }

        if (!isset($elements['#type'])) return;

        // You may want to extend the list below for your specific purposes.

        // Attribute and value must comply with what the html standard
        // says about the resp. element.
        switch ($elements['#type']) {
                case 'textfield':
                case 'textarea':
                case 'password':
                        $attr = 'readonly'; $value = 'readonly';
                break;

                case 'select':
                case 'radio':
                case 'option':
                case 'checkbox':
                case 'submit':
                case 'button':
                case 'file':
                        $attr = 'disabled'; $value = 'disabled';
                break;

                default:
                return;
        }

        if (!isset($elements['#attributes'])) {
                $elements['#attributes'] = array();
                }

        $elements['#attributes'][$attr] = $value;
        }

