<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function hostcare_user_default_permissions() {
  $permissions = array();

  // Exported permission: add email account
  $permissions['add email account'] = array(
    'name' => 'add email account',
    'roles' => array(),
  );

  // Exported permission: add ftp account
  $permissions['add ftp account'] = array(
    'name' => 'add ftp account',
    'roles' => array(),
  );

  // Exported permission: administer hostcare module
  $permissions['administer hostcare module'] = array(
    'name' => 'administer hostcare module',
    'roles' => array(),
  );

  // Exported permission: create hostcare server
  $permissions['create hostcare server'] = array(
    'name' => 'create hostcare server',
    'roles' => array(),
  );

  // Exported permission: create hostcare_server content
  $permissions['create hostcare_server content'] = array(
    'name' => 'create hostcare_server content',
    'roles' => array(),
  );

  // Exported permission: create hostcare_v_server content
  $permissions['create hostcare_v_server content'] = array(
    'name' => 'create hostcare_v_server content',
    'roles' => array(),
  );

  // Exported permission: create hostcare_v_server_account content
  $permissions['create hostcare_v_server_account content'] = array(
    'name' => 'create hostcare_v_server_account content',
    'roles' => array(),
  );

  // Exported permission: delete any hostcare_server content
  $permissions['delete any hostcare_server content'] = array(
    'name' => 'delete any hostcare_server content',
    'roles' => array(),
  );

  // Exported permission: delete any hostcare_v_server content
  $permissions['delete any hostcare_v_server content'] = array(
    'name' => 'delete any hostcare_v_server content',
    'roles' => array(),
  );

  // Exported permission: delete any hostcare_v_server_account content
  $permissions['delete any hostcare_v_server_account content'] = array(
    'name' => 'delete any hostcare_v_server_account content',
    'roles' => array(),
  );

  // Exported permission: delete email account
  $permissions['delete email account'] = array(
    'name' => 'delete email account',
    'roles' => array(),
  );

  // Exported permission: delete ftp account
  $permissions['delete ftp account'] = array(
    'name' => 'delete ftp account',
    'roles' => array(),
  );

  // Exported permission: delete own hostcare_server content
  $permissions['delete own hostcare_server content'] = array(
    'name' => 'delete own hostcare_server content',
    'roles' => array(),
  );

  // Exported permission: delete own hostcare_v_server content
  $permissions['delete own hostcare_v_server content'] = array(
    'name' => 'delete own hostcare_v_server content',
    'roles' => array(),
  );

  // Exported permission: delete own hostcare_v_server_account content
  $permissions['delete own hostcare_v_server_account content'] = array(
    'name' => 'delete own hostcare_v_server_account content',
    'roles' => array(),
  );

  // Exported permission: edit any hostcare_server content
  $permissions['edit any hostcare_server content'] = array(
    'name' => 'edit any hostcare_server content',
    'roles' => array(),
  );

  // Exported permission: edit any hostcare_v_server content
  $permissions['edit any hostcare_v_server content'] = array(
    'name' => 'edit any hostcare_v_server content',
    'roles' => array(),
  );

  // Exported permission: edit any hostcare_v_server_account content
  $permissions['edit any hostcare_v_server_account content'] = array(
    'name' => 'edit any hostcare_v_server_account content',
    'roles' => array(),
  );

  // Exported permission: edit email account
  $permissions['edit email account'] = array(
    'name' => 'edit email account',
    'roles' => array(),
  );

  // Exported permission: edit field_hc_account_owner
  $permissions['edit field_hc_account_owner'] = array(
    'name' => 'edit field_hc_account_owner',
    'roles' => array(),
  );

  // Exported permission: edit field_hc_account_pass
  $permissions['edit field_hc_account_pass'] = array(
    'name' => 'edit field_hc_account_pass',
    'roles' => array(),
  );

  // Exported permission: edit field_hc_account_server
  $permissions['edit field_hc_account_server'] = array(
    'name' => 'edit field_hc_account_server',
    'roles' => array(),
  );

  // Exported permission: edit field_hc_account_type
  $permissions['edit field_hc_account_type'] = array(
    'name' => 'edit field_hc_account_type',
    'roles' => array(),
  );

  // Exported permission: edit field_hc_server_client
  $permissions['edit field_hc_server_client'] = array(
    'name' => 'edit field_hc_server_client',
    'roles' => array(),
  );

  // Exported permission: edit field_hc_server_login
  $permissions['edit field_hc_server_login'] = array(
    'name' => 'edit field_hc_server_login',
    'roles' => array(),
  );

  // Exported permission: edit field_hc_server_moderator
  $permissions['edit field_hc_server_moderator'] = array(
    'name' => 'edit field_hc_server_moderator',
    'roles' => array(),
  );

  // Exported permission: edit field_hc_server_pass
  $permissions['edit field_hc_server_pass'] = array(
    'name' => 'edit field_hc_server_pass',
    'roles' => array(),
  );

  // Exported permission: edit field_hc_server_port
  $permissions['edit field_hc_server_port'] = array(
    'name' => 'edit field_hc_server_port',
    'roles' => array(),
  );

  // Exported permission: edit field_hc_server_ref
  $permissions['edit field_hc_server_ref'] = array(
    'name' => 'edit field_hc_server_ref',
    'roles' => array(),
  );

  // Exported permission: edit ftp account
  $permissions['edit ftp account'] = array(
    'name' => 'edit ftp account',
    'roles' => array(),
  );

  // Exported permission: edit own hostcare server
  $permissions['edit own hostcare server'] = array(
    'name' => 'edit own hostcare server',
    'roles' => array(),
  );

  // Exported permission: edit own hostcare_server content
  $permissions['edit own hostcare_server content'] = array(
    'name' => 'edit own hostcare_server content',
    'roles' => array(),
  );

  // Exported permission: edit own hostcare_v_server content
  $permissions['edit own hostcare_v_server content'] = array(
    'name' => 'edit own hostcare_v_server content',
    'roles' => array(),
  );

  // Exported permission: edit own hostcare_v_server_account content
  $permissions['edit own hostcare_v_server_account content'] = array(
    'name' => 'edit own hostcare_v_server_account content',
    'roles' => array(),
  );

  // Exported permission: view field_hc_account_owner
  $permissions['view field_hc_account_owner'] = array(
    'name' => 'view field_hc_account_owner',
    'roles' => array(),
  );

  // Exported permission: view field_hc_account_pass
  $permissions['view field_hc_account_pass'] = array(
    'name' => 'view field_hc_account_pass',
    'roles' => array(),
  );

  // Exported permission: view field_hc_account_rname
  $permissions['view field_hc_account_rname'] = array(
    'name' => 'view field_hc_account_rname',
    'roles' => array(),
  );

  // Exported permission: view field_hc_account_server
  $permissions['view field_hc_account_server'] = array(
    'name' => 'view field_hc_account_server',
    'roles' => array(),
  );

  // Exported permission: view field_hc_account_type
  $permissions['view field_hc_account_type'] = array(
    'name' => 'view field_hc_account_type',
    'roles' => array(),
  );

  // Exported permission: view field_hc_server_client
  $permissions['view field_hc_server_client'] = array(
    'name' => 'view field_hc_server_client',
    'roles' => array(),
  );

  // Exported permission: view field_hc_server_login
  $permissions['view field_hc_server_login'] = array(
    'name' => 'view field_hc_server_login',
    'roles' => array(),
  );

  // Exported permission: view field_hc_server_moderator
  $permissions['view field_hc_server_moderator'] = array(
    'name' => 'view field_hc_server_moderator',
    'roles' => array(),
  );

  // Exported permission: view field_hc_server_pass
  $permissions['view field_hc_server_pass'] = array(
    'name' => 'view field_hc_server_pass',
    'roles' => array(),
  );

  // Exported permission: view field_hc_server_port
  $permissions['view field_hc_server_port'] = array(
    'name' => 'view field_hc_server_port',
    'roles' => array(),
  );

  // Exported permission: view field_hc_server_ref
  $permissions['view field_hc_server_ref'] = array(
    'name' => 'view field_hc_server_ref',
    'roles' => array(),
  );

  // Exported permission: view hc_server_list-domains
  $permissions['view hc_server_list-domains'] = array(
    'name' => 'view hc_server_list-domains',
    'roles' => array(),
  );

  return $permissions;
}
