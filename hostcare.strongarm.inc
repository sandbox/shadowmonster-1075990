<?php

/**
 * Implementation of hook_strongarm().
 */
function hostcare_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_hostcare_server';
  $strongarm->value = 0;
  $export['comment_anonymous_hostcare_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_hostcare_v_server';
  $strongarm->value = 0;
  $export['comment_anonymous_hostcare_v_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_hostcare_v_server_account';
  $strongarm->value = 0;
  $export['comment_anonymous_hostcare_v_server_account'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_controls_hostcare_server';
  $strongarm->value = '3';
  $export['comment_controls_hostcare_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_controls_hostcare_v_server';
  $strongarm->value = '3';
  $export['comment_controls_hostcare_v_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_controls_hostcare_v_server_account';
  $strongarm->value = '3';
  $export['comment_controls_hostcare_v_server_account'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_hostcare_server';
  $strongarm->value = '4';
  $export['comment_default_mode_hostcare_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_hostcare_v_server';
  $strongarm->value = '4';
  $export['comment_default_mode_hostcare_v_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_hostcare_v_server_account';
  $strongarm->value = '4';
  $export['comment_default_mode_hostcare_v_server_account'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_order_hostcare_server';
  $strongarm->value = '1';
  $export['comment_default_order_hostcare_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_order_hostcare_v_server';
  $strongarm->value = '1';
  $export['comment_default_order_hostcare_v_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_order_hostcare_v_server_account';
  $strongarm->value = '1';
  $export['comment_default_order_hostcare_v_server_account'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_hostcare_server';
  $strongarm->value = '50';
  $export['comment_default_per_page_hostcare_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_hostcare_v_server';
  $strongarm->value = '50';
  $export['comment_default_per_page_hostcare_v_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_hostcare_v_server_account';
  $strongarm->value = '50';
  $export['comment_default_per_page_hostcare_v_server_account'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_hostcare_server';
  $strongarm->value = '0';
  $export['comment_form_location_hostcare_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_hostcare_v_server';
  $strongarm->value = '0';
  $export['comment_form_location_hostcare_v_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_hostcare_v_server_account';
  $strongarm->value = '0';
  $export['comment_form_location_hostcare_v_server_account'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_hostcare_server';
  $strongarm->value = '0';
  $export['comment_hostcare_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_hostcare_v_server';
  $strongarm->value = '0';
  $export['comment_hostcare_v_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_hostcare_v_server_account';
  $strongarm->value = '0';
  $export['comment_hostcare_v_server_account'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_hostcare_server';
  $strongarm->value = '1';
  $export['comment_preview_hostcare_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_hostcare_v_server';
  $strongarm->value = '1';
  $export['comment_preview_hostcare_v_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_hostcare_v_server_account';
  $strongarm->value = '1';
  $export['comment_preview_hostcare_v_server_account'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_hostcare_server';
  $strongarm->value = '1';
  $export['comment_subject_field_hostcare_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_hostcare_v_server';
  $strongarm->value = '1';
  $export['comment_subject_field_hostcare_v_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_hostcare_v_server_account';
  $strongarm->value = '1';
  $export['comment_subject_field_hostcare_v_server_account'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_hostcare_v_server_account';
  $strongarm->value = array(
    'title' => '-5',
    'revision_information' => '6',
    'author' => '7',
    'options' => '8',
    'comment_settings' => '11',
    'menu' => '4',
    'book' => '12',
    'path' => '10',
    'attachments' => '9',
    'domain' => '5',
  );
  $export['content_extra_weights_hostcare_v_server_account'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_hostcare_server';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_hostcare_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_hostcare_v_server';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_hostcare_v_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_hostcare_v_server_account';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_hostcare_v_server_account'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'upload_hostcare_server';
  $strongarm->value = '1';
  $export['upload_hostcare_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'upload_hostcare_v_server';
  $strongarm->value = '1';
  $export['upload_hostcare_v_server'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'upload_hostcare_v_server_account';
  $strongarm->value = '1';
  $export['upload_hostcare_v_server_account'] = $strongarm;

  return $export;
}
