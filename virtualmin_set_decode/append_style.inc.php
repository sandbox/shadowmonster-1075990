<?php
// $Id$

/**
 * @file
 * Hostcare API Module is only Virtualmin connections functions
 *
 * @author Martin Zbozien <martin@hostcare.co.uk>
 * @author Mark Thord <mark.thord@gmail.com>
 * 
 */

/**
 * Convert username from Hostcare to format used by Virtualmin
 * 
 * @param $data_in
 *   data to convert
 * @param $dec
 *   setting returned from Virtualmin
 *
 * @return $data_out
 *  return data after conversion
 *
 */
function virtualmin_decode($data_in, $dec) {

        if ($data_in['title'] != NULL) {
                $reg = array(
                        0 => 'username.domain',
                        1 => 'username-domain',
                        2 => 'domain.username',
                        3 => 'domain-username',
                        4 => 'username_domain',
                        5 => 'domain_username',
                        6 => 'username@domain',
                        7 => 'username%domain'
                        );

                $out = str_replace('username', $data_in['title'], $reg[$dec['settings']['template']['#set_value']]);
                $out = str_replace('domain', $data_in['domain'], $out);

                $data_out['title'] = $out;
                }

        return $data_out;
        }

