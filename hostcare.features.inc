<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function hostcare_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function hostcare_node_info() {
  $items = array(
    'hostcare_server' => array(
      'name' => t('Hostcare Server'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Server Name'),
      'has_body' => '1',
      'body_label' => t('Server Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'hostcare_v_server' => array(
      'name' => t('Hostcare Virtual Server'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Virtual Server Domain'),
      'has_body' => '1',
      'body_label' => t('Virtual Server Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'hostcare_v_server_account' => array(
      'name' => t('Hostcare Virtual Server Account'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Account Name'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function hostcare_views_api() {
  return array(
    'api' => '2',
  );
}
