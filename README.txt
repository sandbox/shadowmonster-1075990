; $Id

This is fresh Developer version of this module. We strongly suggest to do not use
it on live environment!!! This version is only for testing. We recommend use this
on virtualization software ex. VirtualBox.

We test it only on Virtualmin 3.80 GPL.

Virtualmin require root access to remote API (!) and we not recommend use this on
live until Drupal community will confirm that level of security which we use is
enough. To protect root password and also Virtualmin accounts passwords we use
Encrypt module with AES 256 encryption, hostcare require it to can work.

Implemented Functions:
---------------------
* connect with Virtualmin by Remote API
* add/del/edit FTP and Emails accounts with very basic data as login, pass etc.
* fully integrated with CCK



Instruction of Use:
-------------------

        a) turn on Hostcare modules (hostcare, hc_views, hc_account, hostcare_api)
        b) setup permissions - you can use suggested roles or create new ones
           remember that if you give one role add ftp account access you need also
           give access to edit fields (ex. for add ftp or email account minimum for
           account_server, account_type and account_pass).
        c) Create Server node - fill all needed information correctly. After it you
           should see table with all Virtual Servers domains used on your server.
        d) Use 'Convert to Node' option to automatically create node which will
           represent Virtual Server on Hostcare.
           This module do not let create Virtual Servers on Virtualmin yet to avoid
           for now bigger damages for people which do not read module descriptions.
           If admin create Virtual Server node on Drupal it will not do it on 
           Virtualmin
        e) Now you will see two tabs for FTP and Email accounts. If Virtualmin have
           already some accounts you can convert them into representative node.
           If you add account node from Drupal it will aslo add it on VIrtulamin.
           Also Delete and edit mode is working with Virtualmin. 
        f) You can also assign account to user so this user after login will have
           access to login details for this account and change them (if you give them
           access to proper fields). If user do not been yet set-up ftp or email
           account role it will be done automatically.

Please understand that this is very fresh module and first published by us so we are
very happy for any feedback. We still improving our skills on Drupal.

We plan soon:
* implement Virtualmin XML-RPC API
* fully support Emails and FTP Accounts
* implement Virtual Server management with some basic data (on Default Template yet only)
* create installation profile module with suggested permission settings and other
* improve security
* and much more after...


Authors:
--------
Mark Thord <mark.thord@gmail.com>
Martin Zbozien <martin@hostcare.co.uk>


