<?php

/**
 * @file
 * Hostcare API Module is only Virtualmin connections functions
 *
 * @author Martin Zbozien <martin@hostcare.co.uk>
 * @author Mark Thord <mark.thord@gmail.com>
 * 
 */

/**
 * Connect to Virtualmin Remote API
 *
 * Virtualmin Remote API require root access (!) to can use it
 * http://www.virtualmin.com/documentation/developer/http
 * We try at the moment make it hight secure to use root password
 * so we require Encrypt Module with AES encryption be on.
 * 
 * We strongly NOT recommend use it on live environment to time when
 * Drupal community will agreed if this secure level is enough !!!
 * You can use example VirtualBox or other virtualization software
 * without any cost and test it.
 *
 * @param $nid
 *   Node ID with Server data needed to connect
 * @param $command
 *   Base command called for make request to Virtualmin
 * @param $multiline boolean
 *
 * @return $result
 *  return simple data from Virtualmin
 *
 * @see hostcare_nodeapi
 *
 * @todo: better handle errors from Virtualmin
 *   
 */
function hc_connect($nid, $command, $multiline = FALSE) {
        $node = node_load($nid);

        if ($multiline == TRUE) {
                $turn_mult = '&multiline';
                }
                $command = "wget -O - --quiet --http-user=" . $node->field_hc_server_login[0]['value'] . " --http-passwd=" . decrypt($node->field_hc_server_pass[0]['value']) . " --no-check-certificate 'https://" . trim($node->title) . ":" . $node->field_hc_server_port[0]['value'] . "/virtual-server/remote.cgi?program=" . $command . $turn_mult . "'";

//print $command;


        $result = shell_exec($command);

        return $result;
        }

/**
 * Call list-domains command from Virtualmin
 *
 * Virtualmin Remote API require root access (!) to can use it
 *
 * @param $nid int
 *   Node ID with Server data needed to connect
 *
 * @return $out
 *  return simple data with list of domains (virtual Servers)
 * 
 */
function hc_list_servers($nid) {
        $command = 'list-domains';

        $result = hc_connect($nid, $command);

        if (strstr($result, 'ERROR:')) {
                $message = t('Connection has been established but Server respond with error:') . "<br /><i>" . $result . "</i>";
                drupal_set_message(check_plain($message), 'error');
                }
        if (trim($result) == NULL) {
                $message = t('Connection has been established but Server respond with empty value');
                drupal_set_message(check_plain($message), 'error');
                }

        $out = array();
        $out['Server_out'] = $result;
        $out['Server_command'] = $command;

        return $out;
        }

/**
 * Call list-users command from Virtualmin with parametrs
 *
 * Virtualmin Remote API require root access (!) to can use it
 *
 * @param $node object
 *   Node with Server data needed to connect
 *
 * @return $out array
 *  return simple data and command in array
 * 
 */
function hc_list_emailftp_accounts($node, $multiline = FALSE) {
        $command = 'list-users&domain=' . trim($node->title);

        $result = hc_connect($node->field_hc_server_ref['0']['nid'], $command, $multiline);
        //print_r($result);
        $out = array();
        print $out['Server_out'] = $result;
        $out['Server_command'] = $command;

        return $out;
        }

function hc_get_single_account($account, $server_v_node) {

        //TODO: this should return only one user info abut is returning
        //      all records of domain :(
        $command = 'list-users&domain=' . trim($server_v_node->title) . '&user=' . $account;
        $result = hc_connect($server_v_node->field_hc_server_ref['0']['nid'], $command);

        $out = array();
        $out['Server_out'] = $result;
        $out['Server_command'] = $command;

        return $out;
        }

/**
 * Convert data from Virtualmin on better format
 *
 * Virtualmin is back text format but is easy to
 * convert into table format
 *
 * @param $out
 *   Data to convert
 * @param $command
 *   Base of Virtualmin command
 *
 * @return $convert array
 *  return data sorted in array
 * 
 */
function hc_out_convert($out, $command) {
        $out = nl2br($out);
        //print_r($out);
        if (strstr($command, 'list-')) {

                $out = explode('<br />', $out);

                $str_len = explode(" ", $out[1]);

                //take lengh of strings from second row ----
                foreach ($str_len as $key => $value) {
                        $row[] = strlen(trim($value))+1;
                        }

                $convert_key = 0;
                foreach ($out as $k_out => $v_out) {
                        // Data are started from third row and cannot be empty or with exit value
                        if ($k_out > 1 and trim($out[$k_out]) != NULL and !strstr(trim($out[$k_out]), 'Exit status: 0')) {
                                $str_pos = 0;
                                foreach ($row as $r_key => $r_val) {
                                        $convert[$convert_key][trim(substr($out[0], $str_pos, $row[$r_key]))] = trim(substr($out[$k_out], $str_pos, $row[$r_key]));

                                        $str_pos = $str_pos + $row[$r_key];
                                        }
                                $convert_key++;
                                }
                        }

                }

        return $convert;
        }

function hc_out_convert_multiline($out, $command) {
        $out = nl2br($out);
        $out = str_replace("    ", '<record>', $out);
        //$out = str_replace("<br />", '</record>', $out);
        //print_r($out);
        if (strstr($command, 'list-')) {
                $rec_s = explode('<br />', $out);

                //print_r($rec_s);
                $key = 0;
                $records = array();
                foreach ($rec_s as $rec_si => $rec_sv) {
                        if (!strstr($rec_sv, '<record>') and !strstr($rec_sv, 'Exit status') and $rec_sv != NULL) {
                                $key = trim($rec_sv);
                                }
                        elseif (strstr($rec_sv, '<record>')) {
                                $rec_sv = str_replace('<record>', '', $rec_sv);
                                $rec_sv = str_replace("\n", '', $rec_sv);
                                $rec_data = explode(': ', $rec_sv);
                                $records[$key][$rec_data[0]] = $rec_data[1];
                                }
                        }
                }
        $out = $records;
        return $out;
        }

/**
 * Call create-user or modify-user from Virtualmin
 *
 * @param $nid
 *  Node ID with Server data needed to connect
 * @param $data array
 *  Data from Email or FTP account node
 * @param $type strings
 *  Type of account (email or ftp at the moment)
 * @param $modyfi boolean
 *  TRUE if user will be modyfi and FALSE for create new one
 *
 * @return $out
 *  return data from function or errors messages
 * 
 */
function hc_add_account($nid, $data, $type, $modyfi) {

        $extra = '';

        if ($modyfi == TRUE) {
                $com = 'modify-user';
                }
        else {
                $com = 'create-user';
                if ($type == 'ftp') {
                        $extra = '&ftp&noemail';
                        }
                if ($type == 'both') {
                        $extra = '&ftp';
                        }
                if ($type == 'email') {
                        $extra = '';
                        }
                }

        if (trim($data['pass']) != NULL) {
                $pass = '&pass=' . $data['pass'];
                }

        if ($data['disabled'] == 0) {
                $extra .= '&enable';
                }
        elseif ($data['disabled'] == 1) {
                $extra .= '&disable';
                }

        if ($data['quota'] != NULL) {
                        if ($data['quota'] == 'UN') {
                                $kblocks = 'UNLIMITED';
                                }
                        else {
                                //virtualmin want 1Kbs blocks
                                $kblocks = $data['quota']/1024;
                                }
                        $quota = '&quota=' . $kblocks;
                }

        $node = node_load($nid);

        $command = $com . '&domain=' . trim($node->title) . '&user=' . $data['login'] . $pass . '&real=' . $data['rname'] . $quota . $extra;

        $result = hc_connect($node->field_hc_server_ref['0']['nid'], $command);

        if (trim($result) == NULL) {
                $out['error'] = t('Connection has been established but Server respond with empty value');
                }
        if (strstr($result, 'A mailbox or mail alias with the same name and domain already exists')) {
                $out['error'] = t('This username is alredy taken, please choose another');
                }
        if (strstr($result, 'Missing or invalid username')) {
                $out['error'] = t('User Name for account is invalid');
                }

        return $out;
        }

/**
 * Call delete user from Virtualmin
 *
 * @param $nid
 *  Node ID with Server data needed to connect
 * @param $data array
 *  Data from Email or FTP account node
 *
 * @return $out
 *  at the moment do not return nothing
 * 
 * @todo: make confirmation that user has been deleted
 */
function hc_del_account($nid, $data) {
        $node = node_load($nid);

        $command = 'delete-user&domain=' . trim($node->title) . '&user=' . $data['login'];

        $result = hc_connect($node->field_hc_server_ref['0']['nid'], $command);

        return $out;
        }

/**
 * Get settings from Virtual Server Template
 *
 * At the moment we support only 'Default Settings' template
 * It do not support yet fully read all settings
 *
 * @param $server_nid
 *  Node ID with Server data needed to connect
 * @param $tname
 *  At the moment is nothing useful but will be soon
 * @param $setting string
 *  Name of setting used by Virtualmin (ex. append_style)
 *
 * @return $out
 *  at the moment do not return nothing
 * 
 * @see hc_get_account_username as example of use
 */
function hc_get_vserver_template_set($server_nid, $tname = NULL, $setting = NULL) {

        if ($setting != NULL) {
                $this_set = '&setting=' . $setting;
                }
        $command = 'get-template&id=0&inherited' . $this_set;
        
        $result = hc_connect($server_nid, $command);

        if (strstr($result, 'Exit status: 1')) {
                $out['error'] = t('Error. System cannot get settings of template.');
                }
        else {
                if ($setting == NULL) {
                        $rec = explode("\n", $result);
                        foreach ($rec as $rec_i => $rec_v) {
                                $set = explode(":", $rec[$rec_i]);

                                $out['settings']['template'][$rec_i]['#set_name'] = $set[0];
                                $out['settings']['template'][$rec_i]['#set_value'] = $set[1];
                                }
                        }
                else {
                        //delete 'exit status' note
                        $set = explode("\n", $result);
                        $this_set = explode('=', $this_set);
                        $out['settings']['template']['#set_name'] = $this_set[1];
                        $out['settings']['template']['#set_value'] = $set[0];
                        }
                }
        return $out;
        }

/**
 * Decode our setting from Virtualmin template to useful for us data
 *
 * Virtualmin do not send all available types of options to choose 
 * but only this one with are set-up already so we need prepare 
 * to know what returned setting mean for us. Function also 
 * convert data using setting for us.

 * It do not support yet fully read all settings
 *
 * @param $data_in
 *  Just transfer data into decode scheme
 * @param $setting string
 *  Name of setting used by Virtualmin
 * @param $decode
 *  Just transfer decode setting into decode scheme
 *
 * @return
 *  return decoded data using correct decode scheme file
 * 
 * @see hc_get_account_username as example of use
 */
function hc_vserver_setting_decode($data_in, $setting, $decode) {

        $path = drupal_get_path('module', 'hostcare') . '/virtualmin_set_decode';

        if (is_file($path . '/' . $setting . '.inc.php')) {
                include_once($path . '/' . $setting . '.inc.php');
                }
        else {
                include_once($path . '/default.inc.php');
                }

        return virtualmin_decode($data_in, $decode);
        }
